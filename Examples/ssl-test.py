import pdb
import pickle
import socket
import binascii
from ssl import (
    create_default_context,
    PROTOCOL_TLS_CLIENT,
    SSLContext,
    SSLSession,
    OP_NO_SSLv2,
    OP_NO_SSLv3,
    OP_NO_TLSv1_3,
    OP_NO_COMPRESSION
)

def save_ssl_session(sess):
    print("-- Saving TLS session data [File: 'ssl-session'] --")
    with open("ssl-session", "wb") as fp:
        a_sess_id = binascii.b2a_hex(sess.id)
        print("-- Session ID: %s --" % a_sess_id)
        fp.write(b"id = %s\n" % a_sess_id)
        fp.write(b"time = %d\n" % sess.time)
        fp.write(b"timeout = %d\n" % sess.timeout)
        fp.write(b"ticket_lifetime_hint = %d\n" % sess.ticket_lifetime_hint)

def send_head_request(ssock):
    # save TLS session
    ssl_session = ssock.session

    # send some data
    data = b"HEAD / HTTP/1.1\r\nHost: %s\r\n\r\n" % bytes(hostname,encoding='utf8')
    sent = ssock.send(data)
    print("-- Sent [%d/%d bytes] --" % (len(data), sent))
    print()
    for line in data.split(b"\r\n"):
        print(line.decode())

    # receive response
    data = ssock.recv(4096)
    print("-- Received [%d bytes] --" % len(data))
    print()
    for line in data.split(b"\r\n"):
        print(line.decode())

    return ssl_session

hostname = 'www.python.org'

context = create_default_context()
#context.set_ciphers(ciphers)
#context = SSLContext(PROTOCOL_TLS_CLIENT)
context.options |= (OP_NO_SSLv2 | OP_NO_SSLv3 | OP_NO_TLSv1_3 | OP_NO_COMPRESSION)

with socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0) as sock:
    with context.wrap_socket(sock, server_hostname=hostname) as ssock:
        ssock.connect((hostname, 443))
        ssl_session = send_head_request(ssock)

save_ssl_session(ssl_session)

with socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0) as sock:
    print("-- Trying to resume TLS session (inline) --")
    with context.wrap_socket(sock, server_hostname=hostname, session=ssl_session) as ssock:
        ssock.connect((hostname, 443))
        print("-- TLS session %s resumed --"
            % ("WAS" if ssock.session_reused else "WAS NOT"))
        send_head_request(ssock)

#ssl_session = SSLSession()
#ssl_session.time = 1577110186
#ssl_session.timeout = 7200

