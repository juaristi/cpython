import pdb
import pickle
import urllib3
from ssl import SSLSession, SSLObject, SSLSocket, create_default_context

from ssl import (
    OP_NO_SSLv2,
    OP_NO_SSLv3,
    OP_NO_COMPRESSION
)

ciphers = ":".join(
    [
        "ECDHE+AESGCM",
        "ECDHE+CHACHA20",
        "DHE+AESGCM",
        "DHE+CHACHA20",
        "RSA+AESGCM",
        "RSA+AES",
        "!aNULL",
        "!eNULL",
        "!MD5",
        "!DSS"
    ]
)

class MySSLSocket(SSLSocket):
    def do_handshake(self):
        super(SSLSocket, self).do_handshake()
        print("TLS session {} reused".format("was" if self.session_reused else "was not"))

class MyVerifiedHTTPSConnection(urllib3.connection.VerifiedHTTPSConnection):
    def dummy_method(self):
        pass

def ssl_session_getter_callback(host):
    pdb.set_trace()
    print("Requested TLS cached session for hostname '{}'".format(host))
#   ssl_session = None
#   ssl_session = SSLSession()
#   ssl_session.id = b'QF\xe1\xd8\xf5\n\x8fu\xe1\xed\xb6A\xac\x7fa\x824H\xff\xa8\xb8a#\xdd\xa6\xadu\xda\xc1\xd5\x95;'
#   ssl_session.time = 1577110186
#   ssl_session.timeout = 7200
#   ssl_session.ticket_lifetime_hint = 300
#   return ssl_session
#   try:
#       with open('ssl_session', 'rb') as fp:
#           ssl_session = pickle.load(fp)
#       return ssl_session
#   except FileNotFoundError:
#       return None

def ssl_session_setter_callback(host, ssl_session):
    pdb.set_trace()
    print("Got SSLSession object for hostname '{}'".format(host))
    with open('ssl_session', 'wb') as fp:
        pickle.dump(ssl_session, fp)

context = create_default_context()
#context.sslsocket_class = MySSLSocket
#context.sslobject_class = MySSLObject
context.set_ciphers(ciphers)
context.options |= (OP_NO_SSLv2 | OP_NO_SSLv3 | OP_NO_COMPRESSION)
context.post_handshake_auth = False

conn = urllib3.PoolManager(ssl_session_callback=(ssl_session_getter_callback, ssl_session_setter_callback))
#conn = urllib3.PoolManager(ssl_context=context)
#conn = urllib3.PoolManager(ConnectionCls=MyVerifiedHTTPSConnection)
resp = conn.request('GET', 'https://www.veridas.com')

#print("TLS session {} reused".format("was" if socket.session_reused else "was not"))

#sl_session = socket.session
#f ssl_session:
#   pass
#lse:
#   print("TLS session unavailable")

